package com.wordpress.smartsystemdesign.timecardrelease;

/**
 * Created by i54968 on 2/11/15.
 */
public class Time
{
    final static int minutesInHour = 60;
    final int amHours = 12;

    boolean isArmyTime;

    private String timeString;
    private String ampm;

    private int totalTimeInMinutes;
    private int hoursArmy;
    private int hoursAmPm;
    private int minutes;
    public boolean isEmpty;

    // Constructors
    public Time()
    {
        isEmpty = true;
    }

    public Time(int hoursArmy, int minutes, boolean isArmyTime)  // hour is in 24hr format
    {
        initialize(hoursArmy, minutes, isArmyTime);
    }

    public Time(int totalTimeInMinutes, boolean isArmyTime)  // hour is in 24hr format
    {
        initialize(totalTimeInMinutes, isArmyTime);
    }

    public void initialize(int hoursArmy, int minutes, boolean isArmyTime)
    {
        this.isArmyTime = isArmyTime;
        this.hoursArmy = hoursArmy;
        this.minutes = minutes;
        totalTimeInMinutes = hoursArmy * minutesInHour + minutes;

        if(isArmyTime)
        {
            timeString = formatTime24hr(hoursArmy, minutes);
        }
        else
        {
            timeString = formatTime12hr(hoursArmy, minutes);
        }
        isEmpty = false;
    }

    private void initialize(int totalTimeInMinutes, boolean isArmyTime)
    {
        // TODO: Add unit test
        this.totalTimeInMinutes = totalTimeInMinutes;

        hoursArmy = totalTimeInMinutes / minutesInHour;

        if(hoursArmy > 0)  // don't divide by zero
        {
            minutes = totalTimeInMinutes % (hoursArmy * minutesInHour);
        }
        else
        {
            minutes = totalTimeInMinutes;
        }

        if(isArmyTime)
        {
            timeString = formatTime24hr(hoursArmy, minutes);
        }
        else
        {
            timeString = formatTime12hr(hoursArmy, minutes);
        }
        isEmpty = false;
    }

    // Methods
    private String formatTime12hr(int hoursArmy, int minutes)
    {
        if(hoursArmy == amHours || hoursArmy == 0)
        {
            hoursAmPm = 12;  // noon or midnight
        }
        else
        {
            hoursAmPm = hoursArmy % amHours;
        }

        if(hoursArmy >= amHours)
        {
            ampm = "PM";
        }
        else
        {
            ampm = "AM";
        }

        return String.format("%d:%s %s", hoursAmPm, formatMinute(minutes), ampm);  // add spaces between hour, minute, and ampm
    }

    private static String formatTime24hr(int hoursArmy, int minute)
    {
        return String.format("%d:%s", hoursArmy, formatMinute(minute));  // add spaces between hour, minute, and ampm
    }

    private static String formatMinute(int minute)
    {
        final int twoDigitMinute = 10;
        String zeroForSingleDigitMinute;

        if(minute < twoDigitMinute)
        {
            zeroForSingleDigitMinute = "0";
        }
        else
        {
            zeroForSingleDigitMinute = "";
        }
        return String.format("%s%s", zeroForSingleDigitMinute, minute);
    }

    public int subtract(Time timeToSubtract)
    {
        if(isEmpty)
        {
            return 0;
        }
        return (totalTimeInMinutes - timeToSubtract.getTotalTimeInMinutes());
    }

    public static String formatTotalFromMinutes(int totalMinutes)
    {
        int hours = totalMinutes / minutesInHour;
        int minutes = totalMinutes % minutesInHour;

        return formatTime24hr(hours, minutes);
    }

    // Properties
    public int getTotalTimeInMinutes()
    {
        if(isEmpty)
        {
            return 0;
        }
        return totalTimeInMinutes;
    }

    public int getHoursArmy()
    {
        if(isEmpty)
        {
            return 0;
        }
        return hoursArmy;
    }

    public int getHoursAmPm()
    {
        if(isEmpty)
        {
            return 0;
        }
        return hoursAmPm;
    }

    public String getAmpm()
    {
        if(isEmpty)
        {
            return "";
        }
        return ampm;
    }

    public int getMinutes()
    {
        if(isEmpty)
        {
            return 0;
        }
        return minutes;
    }

    public void InitializeTime(int totalTimeInMinutes)
    {
        final int hoursInDay = 24;

        if(totalTimeInMinutes > hoursInDay * minutesInHour)
        {
            initialize(hoursInDay * minutesInHour, false);
        }
        else
        {
            initialize(totalTimeInMinutes, false);
        }
    }

    public void InitializeTime(int hoursArmy, int minutes)
    {
        initialize(hoursArmy, minutes, false);
    }

    public String getTimeString()
    {
        if(isEmpty)
        {
            return "";
        }
        return timeString;
    }

    public void setTime(int timeInMinutes)
    {
        initialize(timeInMinutes, false);  // TODO: convert all isArmyTime parameters in initialize() to a variable based on the phone settings
    }

    public boolean getIsArmyTime()
    {
        return isArmyTime;
    }
}