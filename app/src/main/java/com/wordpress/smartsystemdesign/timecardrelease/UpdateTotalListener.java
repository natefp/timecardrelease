package com.wordpress.smartsystemdesign.timecardrelease;

/**
 * Created by i54968 on 2/5/15.
 */
public interface UpdateTotalListener
{
    public void updateTotal();
}
