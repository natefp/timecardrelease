package com.wordpress.smartsystemdesign.timecardrelease;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper
{
    public static final String TABLE_ENTRIES = "entries";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_START = "start";  // TODO: change these to ints and use totalTimeInMinutes
    public static final String COLUMN_END = "end";

    static final String DATABASE_NAME = "time_Card.db";
    static final int DATABASE_VERSION = 1;

    static final String DATABASE_CREATE = "create table "
            + TABLE_ENTRIES + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_START + " integer, "
            + COLUMN_END + " integer);";

    static final String INSERT_DEFAULT = "insert into "
            + TABLE_ENTRIES + "(" + COLUMN_ID + ", "
            + COLUMN_START + ", " + COLUMN_END + ") "
            + " values (%d, null, null);";

    public SQLiteHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(DATABASE_CREATE);
        for(int i = 0; i < 7; i++)
        {
            String insertString = String.format(INSERT_DEFAULT, i);
            database.execSQL(insertString);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    }
}