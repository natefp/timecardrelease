package com.wordpress.smartsystemdesign.timecardrelease;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Nate Hopkins on 1/30/15.
 */
public class Day implements TimeEntryListener
{
    private TimeEntry startTimeEntry;
    private TimeEntry endTimeEntry;
    private Time startTime;
    private Time endTime;
    private TextView elapsedTimeEntry;
    private int elapsedTime = 0;
    private UpdateTotalListener updateTotalListener;
    public int dayNumber;
    private Activity activity;

    public Day(TimeEntry startTimeEntry, TimeEntry endTimeEntry, TextView elapsedTimeEntry, dayOfWeek day, Activity activity)
    {
        startTime = new Time();
        endTime = new Time();
        this.startTimeEntry = startTimeEntry;
        this.endTimeEntry = endTimeEntry;
        this.elapsedTimeEntry = elapsedTimeEntry;
        this.dayNumber = day.ordinal();
        this.activity = activity;

        this.startTimeEntry.setDay(this);  // give a reference to this day to startTimeEntry
        this.endTimeEntry.setDay(this);
        this.startTimeEntry.setStartOrEnd(StartOrEnd.StartTimeEntry);
        this.endTimeEntry.setStartOrEnd(StartOrEnd.EndTimeEntry);

        this.startTimeEntry.setListener(this);
        this.endTimeEntry.setListener(this);
    }

    public void SetListener(UpdateTotalListener listener)
    {
        updateTotalListener = listener;
    }

    private void calculateRow()
    {
        if(!startTime.isEmpty && !endTime.isEmpty)
        {
            int diff = endTime.subtract(startTime);  // in minutes

            if (diff > 0)
            {
                elapsedTimeEntry.setText(Time.formatTotalFromMinutes(diff));
                elapsedTime = diff;
                updateTotalListener.updateTotal();
            }
            else
            {
                elapsedTimeEntry.setText("End < Start!");
            }
        }
    }

    public void setCurrentDay()
    {
        ((MyActivity) activity).setCurrentDay(dayNumber);
    }

    public void setCurrentStartOrEnd(boolean isStart)
    {
        ((MyActivity) activity).setCurrentStartOrEnd(isStart);
    }

    public void setFocus(boolean isStart)
    {
        ((MyActivity) activity).setFocus(isStart ? startTimeEntry : endTimeEntry);
    }

    public boolean containsStartData()
    {
        if(!startTime.isEmpty)
        {
            return true;
        }
        return false;
    }

    public boolean containsEndData()
    {
        if(!endTime.isEmpty)
        {
            return true;
        }
        return false;
    }

    public int getStartTotalMinutes()
    {
        return startTime.getTotalTimeInMinutes();
    }

    public void setStartTotalMinutes(int startTime)
    {
        if(startTime != 0)
        {
            this.startTime.InitializeTime(startTime);
        }
    }

    public void setStartTime(int hoursArmy, int minutes, boolean isArmyTime)
    {
        startTime.initialize(hoursArmy, minutes, isArmyTime);
    }

    public void setEndTime(int hoursArmy, int minutes, boolean isArmyTime)
    {
        endTime.initialize(hoursArmy, minutes, isArmyTime);
    }

    public String getStartTimeString()
    {
        return startTime.getTimeString();
    }

    public void setStartTime(int value)
    {
        startTime.setTime(value);
    }

    public String getEndTimeString()
    {
        return endTime.getTimeString();
    }

    public void setEndTime(int value)
    {
        endTime.setTime(value);
    }

    public boolean isStartTimeEmpty()
    {
        return startTime.isEmpty;
    }

    public boolean isEndTimeEmpty()
    {
        return endTime.isEmpty;
    }

    public int getEndTotalMinutes()
    {
        return endTime.getTotalTimeInMinutes();
    }

    public void setEndTotalMinutes(int endTime)
    {
        if(endTime != 0)
        {
            this.endTime.InitializeTime(endTime);
        }
    }

    public void refreshStart()
    {
        startTimeEntry.setText(getStartTimeString());
        calculateRow();
    }

    public void refreshEnd()
    {
        endTimeEntry.setText(getEndTimeString());
        calculateRow();
    }

    public int getElapsedTime()
    {
        return elapsedTime;
    }

    public boolean getIsArmyTimeStart()
    {
        return startTime.getIsArmyTime();
    }

    public boolean getIsArmyTimeEnd()
    {
        return endTime.getIsArmyTime();
    }

    @Override
    public void onFocusChange(TimeEntry sender, boolean hasFocus)
    {
        calculateRow();
//        SetDayFocused();
    }
}