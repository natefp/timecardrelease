package com.wordpress.smartsystemdesign.timecardrelease;

/**
 * Created by i54968 on 3/11/15.
 *
 * Allows any instance of TimeEntry to tell whether is is a startTimeEntry or and endTimeEntry.
 */
public enum StartOrEnd
{
    StartTimeEntry, EndTimeEntry  // try putting this in TimeEntry outside of the class
}
