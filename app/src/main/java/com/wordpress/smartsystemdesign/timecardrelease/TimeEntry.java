package com.wordpress.smartsystemdesign.timecardrelease;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.sql.SQLException;


/**
 * Created by i54968 on 2/2/15.
 */
public class TimeEntry extends EditText implements View.OnFocusChangeListener, TextWatcher, TimePickerDialog.OnTimeSetListener
{
    private TimeEntryListener listener;
    private Activity activity;
    private boolean isArmyTime;
    private Day day;
    private StartOrEnd startOrEnd;  // This control is always either a startTimeEntry or an endTimeEntry.  When Day creates each, it initializes this appropriately.

    public TimeEntry(Context context)
    {
        super(context);
        activity = (Activity) context;
        isArmyTime = DateFormat.is24HourFormat(activity);
        this.setOnFocusChangeListener(this);
    }

    public TimeEntry(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        activity = (Activity) context;
        isArmyTime = DateFormat.is24HourFormat(activity);
        this.setOnFocusChangeListener(this);
    }

    public TimeEntry(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        activity = (Activity) context;
        isArmyTime = DateFormat.is24HourFormat(activity);
        this.setOnFocusChangeListener(this);
    }

    public void setListener(TimeEntryListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        String prefKey = getContext().getString(R.string.pref_input_method_key);
        String prefDefault = getContext().getString(R.string.pref_input_method_default);
        String prefPicker = getContext().getString(R.string.pref_picker);
        String prefBoth = getContext().getString(R.string.pref_picker_and_text_entry);

        String inputMethodPref = sharedPrefs.getString(prefKey, prefDefault);

        if(inputMethodPref.equals(prefPicker) || inputMethodPref.equals(prefBoth))
        {
            if (((MyActivity) activity).wasRotated == false)
            {
                if (hasFocus)
                {
                    day.setCurrentDay();
                    day.setCurrentStartOrEnd(((TimeEntry) view).startOrEnd == StartOrEnd.StartTimeEntry ? true : false);
                    showTimePickerDialog(view);
                } else if (view instanceof EditText && listener != null)
                {
                    listener.onFocusChange(this, hasFocus);  // this will calculate time total
                }
            }
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute)
    {
        Entry entry = new Entry();
        entry.setId(day.dayNumber);

        if(startOrEnd == StartOrEnd.StartTimeEntry)
        {
            day.setStartTime(hour, minute, isArmyTime);
            this.setText(day.getStartTimeString());

            entry.setStart(day.getStartTotalMinutes());  // store data from this field in the SQLite database
            updateDatasource(entry);    // TODO: refactor to use day instead of Entry
        }
        else
        {
            day.setEndTime(hour, minute, isArmyTime);
            this.setText(day.getEndTimeString());

            entry.setEnd(day.getEndTotalMinutes());  // store data from this field in the SQLite database
            updateDatasource(entry);
        }
    }

    private void updateDatasource(Entry entry)
    {
        try
        {
            Datasource datasource = new Datasource(activity);
            datasource.open();
            datasource.updateEntry(entry);
            datasource.close();
        }
        catch(SQLException e)
        {
            Log.d("Time Card: ", e.getMessage());
        }
    }

    public void showTimePickerDialog(View view)
    {
        TimePickerFragment timePickerFragment = new TimePickerFragment();

        Bundle bundle = new Bundle();

        if(startOrEnd == StartOrEnd.StartTimeEntry)  // refactor these two ifs into one
        {
            if(day.isStartTimeEmpty() == false)
            {
                bundle.putInt("TotalTimeInMinutes", day.getStartTotalMinutes());
                bundle.putBoolean("IsArmyTime", day.getIsArmyTimeStart());
                timePickerFragment.setArguments(bundle);
            }
        }
        if(startOrEnd == StartOrEnd.EndTimeEntry)
        {
            if (day.isEndTimeEmpty() == false)
            {
                bundle.putInt("TotalTimeInMinutes", day.getEndTotalMinutes());
                bundle.putBoolean("IsArmyTime", day.getIsArmyTimeEnd());
                timePickerFragment.setArguments(bundle);
            }
        }

        timePickerFragment.SetListener(this);
        timePickerFragment.show(activity.getFragmentManager(), "timePicker");
    }

    public boolean isArmyTime()
    {
        return isArmyTime;
    }

    public void setDay(Day day)
    {
        this.day = day;
    }

    public void setStartOrEnd(StartOrEnd startOrEnd)
    {
        this.startOrEnd = startOrEnd;
    }

    public StartOrEnd getStartOrEnd()
    {
        return startOrEnd;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        String prefKey = getContext().getString(R.string.pref_input_method_key);
        String prefDefault = getContext().getString(R.string.pref_input_method_default);
        String prefText = getContext().getString(R.string.pref_text_entry);
        String prefBoth = getContext().getString(R.string.pref_picker_and_text_entry);

        String inputMethodPref = sharedPrefs.getString(prefKey, prefDefault);

        if(inputMethodPref.equals(prefText) || inputMethodPref.equals(prefBoth))
        {
            // Text modification code to constrain format to an appropriate time format
        }
    }
}