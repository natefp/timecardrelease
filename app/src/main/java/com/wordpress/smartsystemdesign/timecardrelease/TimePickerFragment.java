package com.wordpress.smartsystemdesign.timecardrelease;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Created by i54968 on 2/10/15.
 */
public class TimePickerFragment extends DialogFragment
{
    private TimePickerDialog.OnTimeSetListener listener;
    private Time timeInField;

    public void SetListener(TimePickerDialog.OnTimeSetListener listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();

        int hour;
        int minute;
        boolean isArmyTime;

        if(bundle != null && bundle.containsKey("TotalTimeInMinutes"))  // set time from field
        {
            isArmyTime = bundle.getBoolean("IsArmyTime");
            timeInField = new Time(bundle.getInt("TotalTimeInMinutes"), isArmyTime);

            hour = timeInField.getHoursArmy();
            minute = timeInField.getMinutes();
        }
        else
        {
            final Calendar calendar = Calendar.getInstance();  // get current time
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
            isArmyTime = DateFormat.is24HourFormat(getActivity());
        }

        return new TimePickerDialog(getActivity(), listener, hour, minute, isArmyTime);
    }
}