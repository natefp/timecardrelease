package com.wordpress.smartsystemdesign.timecardrelease;

public class Entry
{
    public static int newId = -1;

    long id = newId;  // to keep track of the day (0 is Monday)
    int start;
    int end;
    boolean isStartBlank = true;
    boolean isEndBlank = true;

    public long getId()
    {
        return id;
    }

    public void setId(long value)
    {
        id = value;
    }

    public int getStart()
    {
        return start;
    }

    public void setStart(int value)
    {
        start = value;
        isStartBlank = false;
    }

    public int getEnd()
    {
        return end;
    }

    public void setEnd(int value)
    {
        end = value;
        isEndBlank = false;
    }
}
