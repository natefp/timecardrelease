package com.wordpress.smartsystemdesign.timecardrelease;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

// TODO: Add now button to retrieve the current time.

public class MyActivity extends Activity implements UpdateTotalListener
{

    private TextView total;
    private ArrayList<Day> days = new ArrayList<>();
    public int currentDay;
    public boolean wasRotated;
    private boolean newLaunch;
    public boolean isStartEntryFocused;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);  // set default preferences

        setContentView(R.layout.table_layout);

        TableRow totalRow = (TableRow) findViewById(R.id.TotalRow);
        total = (TextView) totalRow.findViewById(R.id.Total);
        Map<Integer,Entry> entries = null;

        if(savedInstanceState == null)  // Happens once at app launch
        {
            wasRotated = false;
            isStartEntryFocused = true;

            try
            {
                Datasource datasource = new Datasource(this);
                datasource.open();
                entries = datasource.getEntries();
                datasource.close();
            }
            catch(SQLException e)
            {
                Log.d("Time Card: ", e.getMessage());
            }
        }
        else  // was rotated - restore state
        {
            wasRotated = true;
            isStartEntryFocused = savedInstanceState.getBoolean("isStartFocused");
            currentDay = savedInstanceState.getInt("CurrentDay");
        }

        for(dayOfWeek day : dayOfWeek.values())
        {
            int dayResourceId = this.getResources().getIdentifier(day.name(), "id", this.getPackageName());
            int dayResourceString = this.getResources().getIdentifier(day.name(), "string", this.getPackageName());

            TableRow row = (TableRow) findViewById(dayResourceId);  // row is a TableRow defined in day_row.xml
            TextView dayTextView = (TextView) row.getChildAt(0);

            dayTextView.setText(getString(dayResourceString));

            Day tempDay = new Day((TimeEntry) row.getChildAt(1), (TimeEntry) row.getChildAt(2), (TextView) row.getChildAt(3), day, this);

            if(savedInstanceState != null)  // was rotated - restore state
            {
                tempDay.setStartTotalMinutes(savedInstanceState.getIntArray("StartTimes")[day.ordinal()]);
                tempDay.setEndTotalMinutes(savedInstanceState.getIntArray("EndTimes")[day.ordinal()]);
            }
            else  // populate fields from SQLite database
            {
                if(entries.get(day.ordinal()).isStartBlank == false)
                {
                    tempDay.setStartTime(entries.get(day.ordinal()).getStart());
                }
                if(entries.get(day.ordinal()).isEndBlank == false)
                {
                    tempDay.setEndTime(entries.get(day.ordinal()).getEnd());
                }
                newLaunch = true;
            }

            tempDay.SetListener(this);
            days.add(tempDay);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();  // This is where the fields get populated with start or end times from the last day.  This is because all the ids for the edittexts for starttime are the same (same with endtime).  Android tries to repopulate them using the last stored value (Sunday).

        if(wasRotated || newLaunch)
        {
            for (int i = 0; i < days.size(); i++)
            {
                days.get(i).refreshStart();
                days.get(i).refreshEnd();
            }

            days.get(currentDay).setFocus(isStartEntryFocused);

            wasRotated = false;
            newLaunch = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        int[] startTimes = new int[days.size()];
        int[] endTimes = new int[days.size()];

        for(int i = 0; i < days.size(); i++)  // iterate though the days arrayList
        {
            if(days.get(i).containsStartData())
            {
                startTimes[i] = days.get(i).getStartTotalMinutes();
            }

            if(days.get(i).containsEndData())
            {
                endTimes[i] = days.get(i).getEndTotalMinutes();
            }
        }

        outState.putIntArray("StartTimes", startTimes);
        outState.putIntArray("EndTimes", endTimes);
        outState.putBoolean("isStartFocused", isStartEntryFocused);
        outState.putInt("CurrentDay", currentDay);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.action_settings)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateTotal()  // called when a row is able to calculate a new row total and thus the total should be updated
    {
        int tally = 0;

        for(Day day : days)
        {
            tally += day.getElapsedTime();  // total number of minutes worked during week
        }

        total.setText(Time.formatTotalFromMinutes(tally));
    }

    public void setCurrentDay(int currentDay)
    {
        this.currentDay = currentDay;
    }

    public void setCurrentStartOrEnd(boolean isStart)
    {
        isStartEntryFocused = isStart;
    }

    public void setFocus(EditText fieldToFocus)
    {
        fieldToFocus.requestFocus();
    }
}