package com.wordpress.smartsystemdesign.timecardrelease;

/**
 * Created by i54968 on 2/2/15.
 */
public interface TimeEntryListener
{
    public void onFocusChange(TimeEntry sender, boolean hasFocus);
}